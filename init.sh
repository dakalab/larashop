#!/bin/bash
composer install

if [ ! -f .env ]; then
	cp .env.example .env
fi

php artisan key:generate

mlock="/app/migrate.lock"
if [ ! -f $mlock ]; then
	touch $mlock
	php artisan migrate --seed --force
	cronfile="/var/spool/cron/crontabs/root"
	echo "* * * * * cd /app && /usr/bin/php artisan schedule:run >> /dev/null 2>&1" >> $cronfile
fi

php artisan storage:link

chmod -R 0777 storage
