<?php

return [
    '4px_token'     => env('4PX_TOKEN', ''),
    'yuntu_account' => env('YUNTU_ACCOUNT', ''),
    'yuntu_secret'  => env('YUNTU_SECRET', ''),
    'fee'           => env('SHIPPING_FEE', 0),
    'express'       => env('EXPRESS', 'free'),
];
