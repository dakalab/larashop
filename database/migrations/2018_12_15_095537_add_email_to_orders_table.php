<?php

use App\Models\Order;
use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmailToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('email')->nullable()->after('user_id');
            $table->timestamp('confirmed_at')->nullable();
        });

        DB::update('update orders set confirmed_at = created_at where user_id > 0');

        $orders = Order::all();
        foreach ($orders as $order) {
            if ($order->user_id > 0) {
                $user = User::find($order->user_id);
                if ($user && !empty($user->email)) {
                    $order->email = $user->email;
                    $order->save();
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('email');
            $table->dropColumn('confirmed_at');
        });
    }
}
