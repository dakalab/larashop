<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Lenovo AC Adapters',
            'Microsoft AC Adapters',
            'Acer AC Adapters',
            'Schenker AC Adapters',
            'Bose AC Adapters',
            'HP AC Adapters',
            'Clevo AC Adapters',
            'Fujitsu AC Adapters',
            'Packard Bell AC Adapters',
            'Medion AC Adapters',
            'Panasonic AC Adapters',
            'Dell AC Adapters',
            'Sony AC Adapters',
            'Toshiba AC Adapters',
            'Asus AC Adapters',
            'LG AC Adapters',
            'MSI AC Adapters',
            'Samsung AC Adapters',
            'Apple AC Adapters',
        ];
        foreach ($data as $name) {
            $category       = new Category;
            $category->name = $name;
            $category->save();
        }
    }
}
