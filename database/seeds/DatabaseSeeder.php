<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(AttributesTableSeeder::class);
        $this->call(PaypalSettingsTableSeeder::class);
        $this->call(TrackingSettingsTableSeeder::class);
        $this->call(WebsiteSettingsTableSeeder::class);
        $this->call(PageCategoriesTableSeeder::class);
        $this->call(PagesTableSeeder::class);
    }
}
