# LaraShop

E-commerce website based on Laravel 5.6 and adminLTE 2.4

## Installation

Create database:

```
CREATE DATABASE IF NOT EXISTS `larashop` DEFAULT CHARACTER SET utf8mb4;
```

Run `composer install` to install dependencies.

Run `php artisan migrate --seed` to create the DB tables.

Run `php artisan storage:link` to create a symbolic link for images dir.

## Compile JS and CSS

Run `npm run watch` to monitor and automatically recompile your components each time they are modified.

Run `npm run prod` to build the production dist
