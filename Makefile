all: help

# it's important to declare we are using bash, otherwise some make commands may fail
SHELL := /bin/bash

NETWORK=backend
MARIADB_IMG=mariadb:10.4
MARIADB_NAME=larashop-db
MARIADB_PASSWORD=hello123

help:
	###################################################################################
	#
	# conndb            - connect to MariaDB using root
	# logs              - tail the container logs
	# db                - boot up db container
	# db-down           - remove db container
	# network           - create docker bridge network
	# larashop          - boot up larashop container
	# larashop-down     - remove larashop container
	# pingdb            - check database health
	# prune             - run docker system prune
	# prune-data        - [danger] run docker system prune and also remove container volume
	# ps                - list all containers
	# restart           - restart container
	# sh                - enter the container, e.g. make sh c=nginx
	# stats             - show container stats, e.g. make stats c=nginx
	# up                - run docker-compose up
	# down              - run docker-compose down
	# composer          - run composer command
	# yarn              - run yarn command
	#
	###################################################################################
	@echo "Enjoy!"

.PHONY: conndb
conndb:
	docker run -it --rm --network=${NETWORK} ${MARIADB_IMG} bash -c "mysql -A --default-character-set=utf8 -h${MARIADB_NAME} -uroot -p${MARIADB_PASSWORD} larashop"

.PHONY: connredis
connredis:
	docker run --rm -it --net=${NETWORK} ${REDIS_IMG} redis-cli -h ${REDIS_NAME}

.PHONY: logs
logs:
	@docker logs -f --tail=30 larashop

.PHONY: network
network:
	docker network create -d bridge ${NETWORK} || true

.PHONY: pingdb
pingdb:
	@n=1; \
	while [ $${n} -eq 1 ]; \
	do \
		sleep 2s; \
		docker exec -it ${MARIADB_NAME} bash -c "mysql -u root -h 127.0.0.1 -p${MARIADB_PASSWORD} -e 'SELECT 1;'"; \
		n=$$?; \
	done;
	@make print m="maraidb is ready for use";

print:
	@printf '\x1B[32m%s\x1B[0m\n' "$$m"

.PHONY: prune
prune:
	@docker system prune -f

.PHONY: prune-data
prune-data:
	@docker system prune -f --volumes
	@rm migrate.lock

.PHONY: ps
ps:
	docker ps -a

.PHONY: restart
restart:
	docker restart larashop

.PHONY: sh
sh:
	@if [ "$$c" == "" ]; then c=larashop; fi; \
	docker exec -it $$c bash

.PHONY: stats
stats:
	@if [ "$$c" == "" ]; then c=$$(docker ps -a | sed 1d | awk '{print $$NF}'); fi; \
	docker stats $$c

.PHONY: up
up: network
	docker-compose up -d
	make logs

.PHONY: down
down:
	docker-compose down

.PHONY: larashop
larashop: network
	docker-compose up -d larashop

.PHONY: larashop-down
larashop-down:
	docker-compose stop larashop
	docker-compose rm -f larashop

.PHONY: db
db: network
	docker-compose up -d larashop-db

.PHONY: db-down
db-down:
	docker-compose stop larashop-db
	docker-compose rm -f larashop-db

.PHONY: yarn
yarn:
	@if [ "$$c" == "" ]; then c=prod; fi; \
	docker exec larashop yarn $$c

.PHONY: composer
composer:
	@if [ "$$c" == "" ]; then c=update; fi; \
	docker exec larashop composer $$c
